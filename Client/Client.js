var ChannelServerProvider = require('./ChannelServerProvider');

var ClientChannelServer;

var registeredServer;
main();

function main()
{
    var localCommandList = [];
    var channelName = "";
    ClientChannelServer = new ChannelServerProvider();

    var consoleInput = function()
    {
        input(channelName + ">> ", function(data)
        {
            var parts = data.trim().split(' ');
            if(parts[0])
            {
                parts[0] = parts[0].toUpperCase();
                console.log(parts[0]);
            }

            switch (true)
            {
                case (parts[0] == "Q"):
                    process.exit(0);
                    break;
                case (parts[0] == "REG"):
                    
                    var server = parts[1];
                    registeredServer = server;
                    channelName = parts[2];
                    subscriber = parts[3];
                    if(!server || !channelName || !subscriber)
                    {
                        console.log("Missing server or channel. Formst: >> REG ServerIP ChannelName SubscriberID");
                        consoleInput();
                    }
                    else
                    {
                        ClientChannelServer.registerToChannel(server, channelName, subscriber, function()
                        {
                            consoleInput();
                        });
                    }
                    break;
                case (parts[0] == "UNREG"):
                    var serverIP = ClientChannelServer.channelServer;
                    var socket = ClientChannelServer.channelServer.socket;
                    if(socket)
                    {
                        socket.write(data, function(){});
                        socket.once('data', function(d)
                        {
                            console.log("<< " + d);
                        });
                    }
                    else
                    {
                        console.log("No connection found for SLServer " + serverIP);
                    }
                    setTimeout(consoleInput, 300);
                    break;
                case (parts[0] == "MSG"):
                    msg(parts[1], parts[2]);
                    consoleInput();
                    break;
                case (parts[0] == "H"):
                    help();
                    consoleInput();
                    break;
                default:
                    console.log("Command incorrect.");

            }
        });
    };

    console.log("This is the client for sending msg. This is the commands:");
    console.log("==============================================================");
    console.log("REG [server] [channel] [user] => registor to a channel from a server");
    console.log("MSG [msg] => for send a massage in the channel");
    console.log("MSG [user] [msg] => send a massage to a user");
    console.log("===============================================================");
    console.log("Q => exit the app");
    console.log("H => for help");

    consoleInput();
}

function help()
{
    console.log("This is the client for sending msg. This is the commands:");
    console.log("==============================================================");
    console.log("REG [server] [channel] [user] => registor to a channel from a server");
    console.log("MSG [msg] => for send a massage in the channel");
    console.log("MSG [user] [msg] => send a massage to a user");
    console.log("===============================================================");
    console.log("Q => exit the app");
    console.log("H => for help");
}

function msg(user, msg)
{
    var channel = ClientChannelServer.getChannelServerSocket(registeredServer, function(){});
    ClientChannelServer.sendmsg(channel, user, msg);
}
function input(quesstion, callback)
{
    var stdin = process.stdin;
    var stdout = process.stdout;

    stdin.resume();
    stdout.write(quesstion);
    
    stdin.once('data', function(data)
    {
        data = data.toString().trim();
        callback(data);
    })
}