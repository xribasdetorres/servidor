function ChannelServerProvider()
{
    var ServerPort = 43234;

    this.chennelServer = null;
    this.onConnectionClosed = null;
    
    this.registerToChannel = function(server, channel, subscriber, callback)
    {
        var _self = this;
        _self.newSocketToChannelServer(server, function(socket){
            var fnDone = function()
            {
                callback();
            };

            if(socket)
            {
                socket.on('close', function()
                {
                    socket.destroy();
                    if(typeof this.onConnectionClose == "function")
                    {
                        this.onConnectionClosed(_self.channelServer[this.id].server);
                    }
                    delete _self.channelServer;
                    console.log("Connection closed");
                });

                socket.on('error', function()
                {
                    console.log("Connection error");
                });

                socket.on('data', function(data)
                {
                    var msg = data.toString('utf-8').trim().split(' ');
                    if(msg[1] === undefined)
                    {
                        console.log("Server said:" + msg[0]);
                    }
                    else
                    {
                        
                        console.log( msg[0] + " said:" + msg[1]);
                    }
                });

                console.log("Registering '" + subscriber + "' to channel '" + channel + "' in server " + server );
                socket.write("REG " + channel + " " + subscriber + "\n", function()
                {
                    fnDone();
                });

                _self.channelServer = { server: server, socket: socket}
            }
        })
    };

    this.getChannelServerSocket = function(server, callback)
    {
        var socket;
        if(this.channelServer)
        {
            socket = this.channelServer.socket;
        }
        return socket;
    };

    this.sendmsg= function(socket, user, msg)
    {
        
        
        socket.write("MSG " + user + " " + msg, function()
        {
            
        })
    }

    this.newSocketToChannelServer = function(server, callback)
    {
        console.log("Connecting to " + server + " port " + ServerPort);
        var net = require('net');
        var socket = net.Socket();
        socket.id = Math.floor(Math.random() * 10000);
        socket.setNoDelay(true);
        socket.connect(ServerPort, server, function()
        {
            console.log("Connected to " + server);
            callback(socket);
        });
        socket.on('error', function(e)
        {
            console.log("Error connecting to " + server);
            callback(null);
        });
    }
}

module.exports = ChannelServerProvider;