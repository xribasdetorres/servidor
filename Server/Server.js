var net = require('net');
var ClientList = require('./ClientList');

var Port = 43234;
var ClientList;

main();

function main()
{
    ClientList = new ClientList();
    
    initServer(ClientList);
    
    var consoleInput = function()
    {
        input(">> ", function(data)
        {
            var parts = data.trim().split(' ');
            
            if(parts[0])
            {
                parts[0] = parts[0].toUpperCase();
            }

            switch(true)
            {
                case (parts[0] == "Q"):
                    process.exit(0);
                    break;
                case(parts[0] == "LSTCHANELS"):
                    listchannels();
                    consoleInput();
                    break;
                case(parts[0] == "LSTCH"):
                    listuserschannel(parts[1]);
                    consoleInput();
                    break;
                case(parts[0] == "SENDALL"):
                    sendall(parts[1], parts[2]);
                    consoleInput();
                    break;
                case(parts[0] == "H"):
                    help();
                    consoleInput();
                    break;
                default:
                    console.log("Incorrect command");
                    consoleInput();
                    break;
            }
        });
    }

    console.log("This is the server for sending msg. This is the commands:");
    console.log("==============================================================");
    console.log("LSTCH [CHANNEL] => to see all the users from the channel");
    console.log("LSTCHANELS => for send a massage in the channel");
    console.log("SENDALL [channel] [msg] => send a massage to all from a chanel");
    console.log("===============================================================");
    console.log("Q => exit the app");
    console.log("H => for help");
    consoleInput();
}

function help()
{
    console.log("This is the server for sending msg. This is the commands:");
    console.log("==============================================================");
    console.log("LSTCH [CHANNEL] => to see all the users from the channel");
    console.log("LSTCHANELS => for send a massage in the channel");
    console.log("SENDALL [channel] [msg] => send a massage to all from a chanel");
    console.log("===============================================================");
    console.log("Q => exit the app");
    console.log("H => for help");
}

function listchannels()
{
    var channels = [];

    var subscribers = [];

    if(ClientList.getChannels() != null)
    {
        channels = ClientList.getChannels();
        console.log("This is the channels:");
        for(var i in channels)
        {
            console.log(channels[i]);
        }
            
    }
      
}

function listuserschannel(channel)
{
    var subscribers = [];
    subscribers = ClientList.getSubscribers(channel);
    if(subscribers.length > 0)
    {
        console.log("subscribers in channel " + channel + ":");
        for(var i in subscribers)
        {
            console.log(subscribers[i]);
        }
    }
}

function sendall(channels, msg)
{
    var socket = []

    if(ClientList.getSockets(channels) != null)
    {
        socket = ClientList.getSockets(channels);
        
        for(var i in socket)
        {
            ClientList.send(socket[i].socket.id, msg);
        }    
            
    }
}

function initServer(ClientList)
{
    net.createServer(function(sock){
        sock.id = Math.floor(Math.random() * 1000000);
        console.log("Conection received: " + sock.remoteAddress + ":" + sock.remotePort);
        sock.on("data", function(data)
        {
            var _self = this;
            console.log("data " + sock.remoteAddress + ": " + data );
            data.toString('utf-8').split('\n').forEach(function(massage)
            {
                parseData(ClientList, massage, _self, sock);
            })
        });
        sock.on('close', function(data)
        {
            console.log('connection closed for socket ' + sock.id);
            ClientList.removeSocket(sock.id);
        })
        sock.on('error', function(err)
        {
            console.log("Socket error " + sock.id, err.stack);
            ClientList.removeSocket(sock.id);
        })
            ClientList.addClient(null, sock, sock.id);
        console.log('Server listening on port ' + Port);
    }).listen(Port);
}

function parseData(ClientList, massage, ClientSocked, sock)
{
    console.log("Parsing message: " + massage + "/n");
    var parts = massage.trim().split(' ');
    if(parts[0] == "REG")
    {

        var channels = ClientList.getChannels();
        var arechannels = false;

            var game = parts[1];
            var player = parts[2];
            console.log(player);
            ClientList.setChannelToClient(sock.id, player, game);
            ClientList.addSubscrib(sock.id, parts[2]);
        
        
    }
    else if(parts[0] == "MSG")
    {
        console.log(parts[0]);
        console.log(parts[1]);
        console.log(parts[2]);

        if(parts[2] === 'undefined')
        {
            var sockinfo = ClientList.getSocket(sock.id);
            var msg = sockinfo.subscriber + " " + parts[1];
            sendall(sockinfo.channel, msg);
        
        }
        else if(parts[1] === 'undefined'  && parts[2] === 'undefined')
        {

        }
        else
        {
            
            var sockinfo = ClientList.getSocket(sock.id);
            var subscriber = ClientList.getSubscribDetail(parts[1]);
            var msg = sockinfo.subscriber + " " + parts[2];
            ClientList.send(subscriber[0]['id_socket'], msg);
        }
    }    
}

function input(question, callback)
{
    var stdin = process.stdin;
    var stdout = process.stdout;

    stdin.resume();
    stdout.write(question);

    stdin.once('data', function(data)
    {
        data = data.toString().trim();
        callback(data);
    })
}